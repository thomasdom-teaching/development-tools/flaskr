# Docker instructions

## For production

### Step 1: Create an isolated network for your application containers
```shell
# Fill this with the right commands
```

### Step 2: Setup and launch the database

```shell
# Fill this with the right commands

```

### Step 3: Build application image
```shell
# Fill this with the right commands
```

### Step 4 (optional): Initialize database if necessary

```shell
# Fill this with the right commands
```

### Step 5: Launch application
```shell
# Fill this with the right commands
```

### Step 6 (optional): See logs of your application
```shell
# Fill this with the right commands
```

## For development

### RECOMMENDED: Before running docker-compose

Fill this with the right instructions

### Start in foreground

```shell
# Fill this with the right commands
```

### Execute in background

```shell
# Fill this with the right commands
```

### Stop and remove containers

```shell
# Fill this with the right commands
```
