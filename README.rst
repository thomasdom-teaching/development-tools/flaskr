Flaskr
======

The basic blog app built in the Flask `tutorial`_, modified to use
Flask-SQLAlchemy instead of plain SQL.

.. _tutorial: http://flask.pocoo.org/docs/tutorial/


Install
-------

.. code-block:: text

    # clone the repository
    $ git clone https://gitlab.com/thomasdom-teaching/development-tools/flaskr
    $ cd flaskr

If not using Docker, create a virtualenv and activate it:

.. code-block:: text

    $ python3 -m venv venv
    $ . venv/bin/activate

Or on Windows cmd:

.. code-block:: text

    $ py -3 -m venv venv
    $ venv\Scripts\activate.bat

Install Flaskr:

.. code-block:: text

    $ pip install -r requirements.txt


Run
---

.. code-block:: text

    $ export FLASK_APP=flaskr
    $ export FLASK_ENV=development
    $ flask init-db
    $ flask run

Or on Windows cmd:

.. code-block:: text

    > set FLASK_APP=flaskr
    > set FLASK_ENV=development
    > flask init-db
    > flask run

Open http://127.0.0.1:5000 in a browser.


Test
----

.. code-block:: text

    $ pip install -r requirements/development.txt
    $ pytest

Run with coverage report:

.. code-block:: text

    $ coverage run -m pytest
    $ coverage report
    $ coverage html  # open htmlcov/index.html in a browser
